#pragma once

#include <iostream>

class Logger {
public:
    static Logger const& instance();

    template <typename... Ts>
    decltype(auto) log(Ts const&... data) const;

    template <typename... Ts>
    decltype(auto) logError(Ts const&... data) const;

private:
    Logger() = default;
    Logger(Logger const&) = delete;
    Logger& operator=(Logger const&) = delete;
};

template <typename... Ts>
decltype(auto) Logger::log(Ts const&... data) const {
    return (std::cout << ... << data);
}

template <typename... Ts>
decltype(auto) Logger::logError(Ts const&... data) const {
    return (std::cerr << ... << data);
}
