#pragma once

#include "matrix.h"
#include "solution.h"

#include <chrono>
#include <random>

double *expansion(double x0, double d, double alfa, int Nmax, matrix O = 0.0);
solution fib(double a, double b, double epsilon, matrix O = 0.0);
solution lag(double a, double b, double epsilon, int Nmax, matrix O = 0.0);

solution HJ(matrix x0, double s, double alfa, double epsilon, int Nmax, matrix O = 0.0);
solution HJ_trial(solution XB, double s, matrix O = 0.0);
solution Rosen(matrix x0, matrix s0, double alfa, double beta, double epsilon, int Nmax, matrix O = 0.0);
solution sym_NM(matrix x0, double s, double alfa, double beta, double gama, double delta, double epsilon, int Nmax, matrix O = 0.0);

solution SD(matrix x0, double epsilon, int Nmax, matrix O = 0.0);
solution CG(matrix x0, double epsilon, int Nmax, matrix O = 0.0);
solution Newton(matrix x0, double epsilon, int Nmax, matrix O = 0.0);
solution golden(double a, double b, double epsilon, int Nmax, matrix O = 0.0);
double compute_b(matrix x, matrix d, matrix limits);

solution EA(int N, matrix limits, double epsilon, int Nmax, matrix O = 0.0);
