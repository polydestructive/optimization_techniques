#pragma once

void optimize_objective_function(double const step,
                                 double const alpha,
                                 double const beta,
                                 double const gamma,
                                 double const delta,
                                 double const epsilon,
                                 int const n_max,
                                 int const iter_cnt);

// void optimize_real_problem(double const epsilon, int const n_max);
