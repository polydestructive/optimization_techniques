#pragma once

#include "matrix.h"
#include "ode_solver.h"

#include <math.h>

class solution
{
	friend ostream &operator<<(ostream &, const solution &);
public:
	matrix x;
	matrix g; //gradiend
	matrix H; // hesjan
	matrix y; 
	static int f_calls; //licznik wywo�an celu
	static int g_calls; // gradientu
	static int H_calls; //hesjana
	static void clear_calls();  
	solution(double = NAN);
	solution(const matrix &);
	solution(double *, int);
	void fit_fun(matrix = 0.0); //wartosc funkcji celu
	void grad(matrix = 0.0); //gradientu 
	void hess(matrix = 0.0); //hesjana
};
