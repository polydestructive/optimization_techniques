#include "Logger.hpp"

Logger const& Logger::instance() {
    static Logger const logger;
    return logger;
}
