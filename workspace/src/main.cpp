#include "optimize.hpp"

#include <exception>
#include <iostream>

int main() {
    try {
        optimize_objective_function(0.9,  // step (0.1, 0.3, 0.9)
                                    0.5,  // alpha (R: 2.0, SNM: 1.0)
                                    0.5,  // beta
                                    2.0,  // gamma
                                    0.5,  // delta
                                    1e-5, // epsilon
                                    1000, // n_max
                                    100); // iter_cnt
    } catch (std::exception const& exc) {
        std::cerr << "An exception occured: " << exc.what() << "!\n";
    }

    return 0;
}
