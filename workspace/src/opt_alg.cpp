#include "matrix.h"
#include "opt_alg.h"

#include "Logger.hpp"

#include <utility>

double* expansion(double x0, double d, double alfa, int Nmax, matrix O) {
    auto const p = new double[2];
    solution X0{x0};
    solution X1{x0 + d};

    X0.fit_fun();
    X1.fit_fun();

    if (X0.y == X1.y) {
        p[0] = X0.x(0);
        p[1] = X1.x(0);
        return p;
    }

    if (X0.y < X1.y) {
        d *= -1;
        X1.x = X0.x + d;
        X1.fit_fun();

        if (X0.y < X1.y) {
            p[0] = X1.x(0);
            p[1] = X0.x(0) - d;
            return p;
        }
    }

    solution X2;
    auto i = 1;

    while (true) {
        X2.x = x0 + std::pow(alfa, i) * d;
        X2.fit_fun();

        if (X2.y >= X1.y or solution::f_calls >= Nmax) {
            break;
        }

        X0 = X1;
        X1 = X2;
        ++i;
    }

    p[0] = X0.x(0);
    p[1] = X2.x(0);

    if (d <= 0) {
        std::swap(p[0], p[1]);
    }

    return p;
}

solution fib(double a, double b, double epsilon, matrix O) {
    const auto n = static_cast<int>(
        std::ceil(std::log2(std::sqrt(5) * (b - a) / epsilon) /
        std::log2((1 + std::sqrt(5)) / 2)));

    int* const F{new int[n]{1, 1}};

    for (auto i = 2; i < n; ++i) {
        F[i] = F[i - 1] + F[i - 2];
    }

    solution A{a};
    solution B{b};
    solution C;
    solution D;

    C.x = B.x - 1.0 * F[n - 2] / F[n - 1] * (B.x - A.x);
    D.x = A.x + B.x - C.x;

    C.fit_fun();
    D.fit_fun();

    for (auto i = 0; i <= n - 3; ++i) {
        // Logger::instance().log("f ", i, ' ', B.x - A.x, '\n');

        if (C.y < D.y) {
            B = D;
        } else {
            A = C;
        }

        C.x = B.x - 1.0 * F[n - i - 2] / F[n -i - 1] * (B.x - A.x);
        D.x = A.x + B.x - C.x;

        C.fit_fun();
        D.fit_fun();
    }

    return C;
}

solution lag(double a, double b, double epsilon, int Nmax, matrix O) {
    solution A{a};
    solution B{b};
    solution C;
    solution D;

    C.x = (a + b) / 2;

    A.fit_fun();
    B.fit_fun();
    C.fit_fun();

    auto l = 0.0;
    auto m = 0.0;
    // auto i = -1;

    while (true) {
        // Logger::instance().log("l ", ++i, ' ', B.x - A.x, '\n');
        l = A.y(0) * (std::pow(C.x(0), 2) - std::pow(B.x(0), 2)) +
            B.y(0) * (std::pow(A.x(0), 2) - std::pow(C.x(0), 2)) +
            C.y(0) * (std::pow(B.x(0), 2) - std::pow(A.x(0), 2));
        m = A.y(0) * (C.x(0) - B.x(0)) +
            B.y(0) * (A.x(0) - C.x(0)) + C.y(0) * (B.x(0) - A.x(0));
        D.x = 0.5 * l / m;

        D.fit_fun();

        if (D.x < C.x) {
            if (D.y < C.y) {
                B = C;
                C = D;
            } else {
                A = D;
            }
        } else {
            if (D.y < C.y) {
                A = C;
                C = D;
            } else {
                B = D;
            }
        }

        if (B.x - A.x < epsilon or solution::f_calls >= Nmax) {
            return C;
        }
    }
}

solution HJ(matrix x0, double s, double alfa, double epsilon, int Nmax, matrix O) {
    solution XB;
    solution XB_old;
    solution X;

    XB.x = x0;
    XB.fit_fun();

    Logger::instance().logError("i_hj x1*_hj x2*_hj\n");

    auto i = 1;

    while (true) {
        Logger::instance().logError(i++, ' ', XB.x(0), ' ', XB.x(1), '\n');

        X = HJ_trial(XB, s);

        if (X.y < XB.y) {
            while (true) {
                XB_old = XB;
                XB = X;
                X.x = 2.0 * XB.x - XB_old.x;
                X.fit_fun();
                X = HJ_trial(X, s);

                if (X.y >= XB.y) {
                    break;
                }

                if (solution::f_calls > Nmax) {
                    return XB;
                }
            }
        } else {
            s *= alfa;
        }

        if (s < epsilon or solution::f_calls > Nmax) {
            return XB;
        }
    }
}

solution HJ_trial(solution XB, double s, matrix O) {
    auto const n = get_size(XB.x);
    matrix const D{unit_mat(n[0])};
    solution X;

    for (auto i = 0; i < n[0]; ++i) {
        X.x = XB.x + s * D[i];
        X.fit_fun();

        if (X.y < XB.y) {
            XB = X;
        } else {
            X.x = XB.x - s * D[i];
            X.fit_fun();

            if (X.y < XB.y) {
                XB = X;
            }
        }
    }

    return XB;
}

solution Rosen(matrix x0, matrix s0, double alfa, double beta, double epsilon, int Nmax, matrix O) {
    auto const n = get_size(x0);
    matrix l{n[0], 1};
    matrix p{n[0], 1};
    matrix s{s0};
    matrix D{unit_mat(n[0])};
    solution X;
    solution Xt;

    X.x = x0;
    X.fit_fun();

    Logger::instance().logError("i_r x1*_r x2*_r\n");

    auto i = 1;

    while (true) {
        Logger::instance().logError(i++, ' ', X.x(0), ' ', X.x(1), '\n');

        for (auto i = 0; i < n[0]; ++i) {
            Xt.x = X.x + s(i) * D[i];
            Xt.fit_fun();

            if (Xt.y < X.y) {
                X = Xt;
                l(i) += s(i);
                s(i) *= alfa;
            } else {
                ++p(i);
                s(i) *= -beta;
            }
        }

        bool change = true;

        for (auto i = 0; i < n[0]; ++i) {
            if (l(i) == 0 or p(i) == 0) {
                change = false;
                break;
            }
        }

        if (change) {
            matrix Q{n[0], n[0]};
            matrix v{n[0], 1};

            for (auto i = 0; i < n[0]; ++i) {
                for (auto j = 0; j <= i; ++j) {
                    Q(i, j) = l(i);
                }
            }

            Q = D * Q;
            v = Q[0] / norm(Q[0]);
            D = set_col(D, v, 0);

            for (auto i = 1; i < n[0]; ++i) {
                matrix temp{n[0], 1};

                for (auto j = 0; j < i; ++j) {
                    temp = temp + trans(Q[i]) * D[j] * D[j];
                }

                v = (Q[i] - temp) / norm(Q[i] - temp);
                D = set_col(D, v, i);
            }

            s = s0;
            l = matrix(n[0], 1);
            p = matrix(n[0], 1);
        }

        auto  max_s = std::abs(s(0));

        for (auto i = 1; i < n[0]; ++i) {
            if (max_s < std::abs(s(i))) {
                max_s = std::abs(s(i));
            }
        }

        if (max_s < epsilon or solution::f_calls > Nmax) {
            return X;
        }
    }
}
solution sym_NM(matrix x0, double s, double alfa, double beta, double gama, double delta, double epsilon, int Nmax, matrix O) {
    auto const n = get_size(x0);
    matrix D{unit_mat(n[0])};
    auto N = n[0] + 1;
    auto S = new solution[N];

    S[0].x = x0;
    S[0].fit_fun();

    for (auto i = 1; i < N; ++i) {
        S[i].x = S[0].x + s * D[i - 1];
        S[i].fit_fun();
    }

    solution p_o;
    solution p_e;
    solution p_z;
    matrix p_sr;
    int i_min;
    int i_max;

    Logger::instance().logError("i_snm x1*_snm x2*_snm\n");

    auto i = 1;

    while (true) {
        i_min = i_max = 0;

        for (auto i = 1; i < N; ++i) {
            if (S[i_min].y > S[i].y) {
                i_min = i;
            }
            if (S[i_max].y < S[i].y) {
                i_max = i;
            }
        }

        p_sr = matrix(n[0], 1);

        for (auto i = 0; i < N; ++i) {
            if (i not_eq i_max) {
                p_sr = p_sr + S[i].x;
            }
        }

        p_sr = p_sr / (N - 1.0);
        p_o.x = p_sr + alfa * (p_sr - S[i_max].x);
        p_o.fit_fun();

        if (S[i_min].y <= p_o.y && p_o.y < S[i_max].y) {
            S[i_max] = p_o;
        } else if (p_o.y < S[i_min].y) {
            p_e.x = p_sr + gama * (p_o.x - p_sr);
            p_e.fit_fun();

            if (p_e.y < p_o.y) {
                S[i_max] = p_e;
            } else {
                S[i_max] = p_o;
            }
        } else {
            p_z.x = p_sr + beta * (S[i_max].x - p_sr);
            p_z.fit_fun();

            if (p_z.y < S[i_max].y) {
                S[i_max] = p_z;
            } else {
                for (auto i = 0; i < N; ++i) {
                    if (i not_eq i_min) {
                        S[i].x = delta * (S[i].x + S[i_min].x);
                        S[i].fit_fun();
                    }
                }
            }
        }

        auto max_s{norm(S[0].x - S[i_min].x)};

        for (int i = 1; i < N; ++i) {
            if (max_s < norm(S[i].x - S[i_min].x)) {
                max_s = norm(S[i].x - S[i_min].x);
            }
        }

        Logger::instance().logError(i++, ' ', S[i_min].x(0), ' ',
                                    S[i_min].x(1), '\n');

        if (max_s < epsilon or solution::f_calls > Nmax) {
            return S[i_min];
        }
    }
}

// LAB 3

/*
solution SD(matrix x0, double epsilon, int Nmax, matrix O)
{
	int *n = get_size(x0);
	solution X, X1;
	X.x = x0;
	matrix d(n[0], 1), P(n[0], 2), limits = O;
	solution h;
	double b;
	while (true)
	{
		X.grad();
		d =
			P = set_col(P, X.x, 0);
		P = set_col(P, d, 1);
		b = compute_b(, , limits);
		h = golden(, , epsilon, Nmax, P);
		X1.x =
			if ()
			{
				X1.fit_fun();
				return X1;
			}
		X =
	}
}

solution CG(matrix x0, double epsilon, int Nmax, matrix O)
{
	int *n = get_size(x0);
	solution X, X1;
	X.x = x0;
	matrix d(n[0], 1), P(n[0], 2), limits = O;
	solution h;
	double b, beta;
	X.grad();
	d =
		while (true)
		{
			P = set_col(P, X.x, 0);
			P = set_col(P, d, 1);
			b = compute_b(, , limits);
			h = golden(, , epsilon, Nmax, P);
			X1.x =
				if ()
				{
					X1.fit_fun();
					return X1;
				}
			X1.grad();
			beta =
				d =
				X =
		}
}

solution Newton(matrix x0, double epsilon, int Nmax, matrix O)
{
	int *n = get_size(x0);
	solution X, X1;
	X.x = x0;
	matrix d(n[0], 1), P(n[0], 2), limits = O;
	solution h;
	double b;
	while (true)
	{
		X.grad();
		X.hess();
		d =
			P = set_col(P, X.x, 0);
		P = set_col(P, d, 1);
		b = compute_b(, , limits);
		h = golden(, , epsilon, Nmax, P);
		X1.x =
			if ()
			{
				X1.fit_fun();
				return X1;
			}
		X =
	}
}

solution golden(double a, double b, double epsilon, int Nmax, matrix O)
{
	double alfa =
		solution A, B, C, D;
	A.x = a;
	B.x = b;
	C.x =
		C.fit_fun(O);
	D.x =
		D.fit_fun(O);
	while (true)
	{
		if ()
		{
			B =
				D =
				C.x =
				C.fit_fun(O);
		}
		else
		{
			A =
				C =
				D.x =
				D.fit_fun(O);
		}
		if ()
		{
			A.x = (A.x + B.x) / 2.0;
			A.fit_fun(O);
			return A;
		}
	}
}

double compute_b(matrix x, matrix d, matrix limits)
{
	int *n = get_size(x);
	double b = 1e9, bi;
	for (int i = 0; i < n[0]; ++i)
	{
		if (d(i) == 0)
			bi =
		else if (d(i) > 0)
			bi =
		else
			bi =
			if (b > bi)
				b = bi;
	}
	return b;
}
*/

// LAB 4

/*
solution EA(int N, matrix limits, double epsilon, int Nmax, matrix O)
{
	int mi = 20, lambda = 40;
	solution *P = new solution[mi + lambda];
	solution *Pm = new solution[mi];
	random_device rd;
	default_random_engine gen;
	gen.seed(static_cast<unsigned int>(chrono::system_clock::now().time_since_epoch().count()));
	normal_distribution<double> distr(0.0, 1.0);
	matrix IFF(mi, 1), temp(N, 2);
	double r, s, s_IFF;
	double tau =, tau1 = ;
	int j_min;
	for (int i = 0; i <; ++i)
	{
		P[i].x = matrix(N, 2);
		for (int j = 0; j < N; ++j)
		{
			P[i].x(j, 0) =
				P[i].x(j, 1) =
		}
		P[i].fit_fun();
		if (P[i].y < epsilon)
			return P[i];
	}
	while (true)
	{
		s_IFF = 0;
		for (int i = 0; i <; ++i)
		{
			IFF(i) =
				s_IFF =
		}
		for (int i = 0; i <; ++i)
		{
			r =
				s = 0;
			for (int j = 0; j <; ++j)
			{
				s += IFF(j);
				if (r <= s)
				{
					P[mi + i] = P[j];
					break;
				}
			}
		}
		for (int i = 0; i <; ++i)
		{
			r = distr(gen);
			for (int j = 0; j < N; ++j)
			{
				P[mi + i].x(j, 1) =
					P[mi + i].x(j, 0) =
			}
		}
		for (int i = 0; i <; i += 2)
		{
			r =
				temp = P[mi + i].x;
			P[mi + i].x =
				P[mi + i + 1].x =
		}
		for (int i = 0; i <; ++i)
		{
			P[mi + i].fit_fun();
			if (P[mi + i].y < epsilon)
				return P[mi + i];
		}
		for (int i = 0; i < mi; ++i)
		{
			j_min = 0;
			for (int j = 1; j <; ++j)
				if (P[j_min].y>P[j].y)
					j_min = j;
			Pm[i] = P[j_min];
			P[j_min].y = 1e10;
		}
		for (int i = 0; i < mi; ++i)
			P[i] = Pm[i];
		if ()
			return P[0];
	}
}
*/
