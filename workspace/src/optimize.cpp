#include "matrix.h"
#include "ode_solver.h"
#include "opt_alg.h"

#include "Logger.hpp"

#include <chrono>
#include <random>

void optimize_objective_function(double const step,
                                 double const alpha,
                                 double const beta,
                                 double const gamma,
                                 double const delta,
                                 double const epsilon,
                                 int const n_max,
                                 int const iter_cnt) {
    matrix x{2, 1};
    matrix steps_in_two_axes{2, 1};

    steps_in_two_axes(0) = step;
    steps_in_two_axes(1) = step;

    std::default_random_engine eng;
    std::uniform_real_distribution<double> dist{-1, 1};

    eng.seed(std::chrono::system_clock::now().time_since_epoch().count());

    Logger::instance().log("x1 x2 "
                           "x1*_hj x2*_hj y*_hj f_calls_hj "
                           "x1*_r x2*_r y*_r f_calls_r "
                           "x1*_snm x2*_snm y*_snm f_calls_snm\n");

    for (auto i = 0; i < iter_cnt; ++i) {
        x(0) = dist(eng); // x axis
        x(1) = dist(eng); // y axis

        Logger::instance().log(x(0), ' ', x(1), ' ');

        Logger::instance().log(HJ(x,
                                  step,
                                  alpha,
                                  epsilon,
                                  n_max));
        solution::clear_calls();


        Logger::instance().log(Rosen(x,
                                     steps_in_two_axes,
                                     alpha,
                                     beta,
                                     epsilon,
                                     n_max));
        solution::clear_calls();

        Logger::instance().log(sym_NM(x,
                                      step,
                                      alpha,
                                      beta,
                                      gamma,
                                      delta,
                                      epsilon,
                                      n_max));
        solution::clear_calls();

        Logger::instance().log('\n');
    }
}

/* void optimize_real_problem(double const epsilon, int const n_max) {
} */
