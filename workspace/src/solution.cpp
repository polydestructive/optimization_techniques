//Do not edit the code below (unless you know what you are doing)

#include "solution.h"

#include "Logger.hpp"

#include <cmath>
#include <ostream>

int solution::f_calls = 0;
int solution::g_calls = 0;
int solution::H_calls = 0;

solution::solution(double L)
{
	x = matrix(L);
	g = NAN;
	H = NAN;
	y = NAN;
}

solution::solution(const matrix &A)
{
	x = A;
}

solution::solution(double *A, int n)
{
	x = matrix(A, n);
	g = NAN;
	H = NAN;
	y = NAN;
}

void solution::clear_calls()
{
	f_calls = 0;
	g_calls = 0;
	H_calls = 0;
}

std::ostream& operator<<(std::ostream& os, solution const& A) {
    Logger::instance().log(A.x, ' ', A.y, ' ', solution::f_calls, ' ');
    return os;
}

void solution::fit_fun(matrix O) {
    auto const pi = std::acos(-1.0);

    // 1. Test objective function
    y = std::pow(x(0), 2) + std::pow(x(1), 2) -
        std::cos(2.5 * pi * x(0)) - std::cos(2.5 * pi * x(1)) + 2;

    /* auto const a_ref{3.14};
    auto const o_ref{0.0};
    matrix Y0{2, 1};
    auto const Y = solve_ode(0, 0.1, 100, Y0, x);
    auto const n = get_size(Y[1]);

    y(0) = 0.0;

    for (auto i = 0; i < n[0]; ++i) {
        y = y + 10 * std::pow(a_ref - Y[1](i, 0), 2) +
                     std::pow(o_ref - Y[1](i, 1), 2) +
                     std::pow(x(0) * (a_ref - Y[1](i, 0)) +
                              x(1) * (o_ref - Y[1](i, 1)), 2);
    }

    y = y * 0.1; */

    ++f_calls;
}

void solution::grad(matrix O)
{
	g = NAN;
	++g_calls;
}

void solution::hess(matrix O)
{
	H = NAN;
	++H_calls;
}
